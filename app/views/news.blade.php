@section('content')
	<div class="container" ng-app="newApp" ng-controller="mainNewController">
		<div class="col-md-8 col-md-offset-2">

			<div class="page-header">
				<h2>Laravel and Angular <b>Triple</b> Page Application</h2>
				<h4>News System</h4>
			</div>

			<!-- NEW NEW FORM -->
			<form ng-submit="submitNew()"> <!-- ng-submit will disable the default form action and use our function -->
				<!-- AUTHOR -->
				<div class="form-group">
					<input type="text" class="form-control input-sm" name="author" ng-model="newData.author" placeholder="Name">
				</div>
				<!-- NEW TEXT -->
				<div class="form-group">
					<input type="text" class="form-control input-lg" name="new" ng-model="newData.text" placeholder="Say what you have to say">
				</div>
				<!-- SUBMIT BUTTON -->
				<div class="form-group text-right">	
					<button type="submit" class="btn btn-primary btn-lg">Submit</button>
				</div>
			</form>

			<pre>
			<% newData %>
			</pre>

			<!-- LOADING ICON -->
			<!-- show loading icon if the loading variable is set to true -->
			<p class="text-center" ng-show="loading"><span class="fa fa-meh-o fa-5x fa-spin"></span></p>

			<!-- THE NEWS -->
			<!-- hide these news if the loading variable is true -->
			<div class="news" ng-hide="loading" ng-repeat="new in news">
				<h3>News #<% new.id %> <small>by <% new.author %></h3>
				<p><% new.text %></p>

				<p><a href="#" ng-click="deleteNew(new.id)" class="text-muted">Delete</a></p>
			</div>

		</div>
	</div>
@stop