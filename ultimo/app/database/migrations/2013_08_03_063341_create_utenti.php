<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUtenti extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('utenti', function(Blueprint $table)
		{
			$table->increments('id');
		  	$table->string('username');
		  	$table->string('password',255);			
 		});

        DB::table('utenti')->insert(
                array(
                    'username' => 'frank'
                    ,'password' => Hash::make('pistillo')                      
                )); 	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('utenti');
	}

}
