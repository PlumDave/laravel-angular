var commentApp = angular.module('commentApp', ['mainCtrl', 'commentService'], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
});

var postApp = angular.module('postApp', ['mainPostCtrl', 'postService'], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
});

var postApp = angular.module('tagApp', ['mainTagCtrl', 'tagService'], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
});

var linksApp = angular.module('linksApp', ['mainLinksCtrl', 'linksService'], function($interpolateProvider) {
  $interpolateProvider.startSymbol('<%');
  $interpolateProvider.endSymbol('%>');
});


var categoryApp = angular.module('categoryApp', ['mainCategoryCtrl', 'categoryService'], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
});

var utentiApp = angular.module('utentiApp', ['mainUtentiCtrl', 'utentiService'], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
});
