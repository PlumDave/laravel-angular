@section('content')
	<div class="container" ng-app="postApp" ng-controller="mainPostController">

		<div class="row">


				<div class="page-header">
					<h2>Laravel and Angular <b>Double</b> Page Application</h2>
					<h4>Posts System</h4>
				</div>

				<div class="col-lg-8">

				<form ng-submit="submitPost()">

					<!-- AUTHOR -->
					<div class="form-group">
						<input type="text" class="form-control input-sm" name="author" ng-model="postData.author" placeholder="Name">
					</div>

					<!-- TEXT -->
					<div class="form-group">
						<input type="text" class="form-control input-lg" name="post" ng-model="postData.text" placeholder="Say what you have to say">
					</div>

					<!-- OTHER FIELD -->
					<div class="form-group">
						<input type="text" class="form-control input-lg" name="post" ng-model="postData.other_field" placeholder="Other input">
					</div>

					<!-- SUBMIT BUTTON -->
					<div class="form-group text-right">
						<button type="submit" class="btn btn-primary btn-lg">Submit</button>
					</div>
				</form>
			</div>

			<!-- OBJECT DISPLAY -->
			<div class="col-lg-4">
				<pre>
				<% postData %>
				</pre>
			</div>

		</div>

		<!-- ALERT -->
		<div class="row">
			<div class="col-md-12">
				<div ng-show="alert.display" class="alert alert-success" role="alert">
					<% alert.message %>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-6 col-md-6">

				<!-- LOADING -->
				<!-- show loading icon if the loading variable is set to true -->
				<p class="text-center" ng-show="loading"><span class="fa fa-meh-o fa-5x fa-spin"></span></p>

				<!-- THE POSTS -->
				<!-- hide these posts if the loading variable is true -->
				<table class="table table-striped posts">
					<tr ng-hide="loading" ng-repeat="post in posts">
						<td><a ng-click="showPost(post.id)" ><h3>Post #<% post.id %> <small>by <% post.author %></h3></a></td>
						<td><p><% post.text %></p></td>
						<td><p><% post.other_field %></p></td>
						<td><p><a ng-click="deletePost(post.id)" class="text-muted">Delete</a></p></td>
					</tr>
				</table>

			</div>

			<div class="col-lg-6 col-md-6">

				<form ng-submit="updatePost(singlepost)">

					<!-- AUTHOR -->
					<div class="form-group">
						<input type="text" class="form-control input-sm" name="author" ng-model="singlepost.author" placeholder="Author">
					</div>

					<!-- TEXT -->
					<div class="form-group">
						<input type="text" class="form-control input-sm" name="author" ng-model="singlepost.text" placeholder="Text">
					</div>

					<!-- OTHER FIELD -->
					<div class="form-group">
						<textarea type="text" class="form-control input-lg" name="category_other" ng-model="singlepost.other_field" placeholder="Other input"></textarea>
					</div>
					
					<!-- SUBMIT BUTTON -->
					<div class="form-group text-right">
						<button type="submit" class="btn btn-primary btn-lg">Save</button>
					</div>
				</form>

			</div>
		</div>
	</div>
@stop
