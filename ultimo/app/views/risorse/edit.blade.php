@section('content')

{{ Form::open(array('url' => 'risorse/'. $risorse_dettaglio->id, 'method' => 'PUT', 'files' => true )) }}
<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		{{ Form::label('titolo_risorsa', 'Titolo risorsa') }}
		{{ Form::text('titolo_risorsa', $risorse_dettaglio->titolo_risorsa, array('class'=>'form-control')) }}
		</div>
	</div>
</div> 

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		{{ Form::label('descrizione_risorsa', 'Descrizione risorsa') }}
		{{ Form::textarea('descrizione_risorsa', $risorse_dettaglio->descrizione_risorsa, array('class'=>'form-control')) }}
		</div>
	</div>
</div> 

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		<a href="{{ url($risorse_dettaglio->url_risorsa) }}" target="_blank">Link risorsa</a>
		    
		{{ Form::label('url_risorsa', 'URL risorsa (lascia vuoto se non vuoi aggiornare)') }}
		{{ Form::file('url_risorsa', '', array('class'=>'form-control')) }}
		</div>
	</div>
</div> 

 <div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		 @foreach($categorie_lista as $c)
		 	@if ( in_array($c['id'] , $categorie_scelte) )
		 		<?php $vero_falso = true ?>
		 	@else
		 		<?php $vero_falso = false ?>
		 	@endif
		<div class="checkbox">		 
		  <label>
		  {{ Form::checkbox('categorie[]', $c['id'] , $vero_falso) }}		    
		  {{ $c['nome_categoria'] }}
		  </label>		 
		</div>	
		 @endforeach	
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
			{{ Form::submit('Aggiungi questa risorsa',  array('class' =>'btn btn-success btn-large')) }}
		</div>
	</div>
</div>
{{ Form::close() }}
@stop