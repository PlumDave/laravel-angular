@section('content')
<div class="row">
	<div class="col-lg-12">
		<span class="pull-right">
			<a href="{{ url('tag/create') }}" class="btn btn-success">
				Nuovo Tag
			</a>
		</span>
	</div>
</div>

<hr>

<div class="row">
	<div class="col-lg-12">
		<table class="table table-striped">
		<tr class="success">
			<td>Tag</td>
			<td>Azioni</td>
		</tr>
		@foreach($tag_lista as $c)
		<tr>
			<td>{{ $c['nome_tag'] }}</td>
			<td>
			<a href="{{ url('tag/'.$c['id'].'/edit') }}" class="btn btn-warning">Modifica</a> 
			<a href="{{ url('tag/'.$c['id']) }}" class="btn btn-warning">Cancella</a>
			</td>
		</tr>
		@endforeach
		</table>
	</div>
</div>
@stop