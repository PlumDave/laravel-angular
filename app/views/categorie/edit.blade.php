@section('content')

{{ Form::open(array('url' => 'categorie/'. $categoria_dettaglio->id, 'method' => 'PUT')) }}
<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		{{ Form::label('nome_categoria', 'Nome categoria') }}
		{{ Form::text('nome_categoria', $categoria_dettaglio->nome_categoria, array('class'=>'form-control')) }}
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		{{ Form::label('descrizione', 'Descrizione categoria') }}
		{{ Form::text('descrizione', $categoria_dettaglio->descrizione, array('class'=>'form-control')) }}
		</div>
	</div>
</div> 

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
			{{ Form::submit('Aggiorna questa categoria',  array('class' =>'btn btn-success btn-large')) }}
		</div>
	</div>
</div>
{{ Form::close() }}
@stop