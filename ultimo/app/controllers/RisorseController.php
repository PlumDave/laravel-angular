<?php

class RisorseController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['risorse_lista'] = Risorse::paginate(4);
		$this->layout->content = View::make('risorse.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['categorie_lista'] = Categorie::all();
		$this->layout->content = View::make('risorse.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = array(
				'titolo_risorsa' => Input::get('titolo_risorsa')
				,'descrizione_risorsa' => Input::get('descrizione_risorsa')
				,'categorie.0' => Input::get('categorie.0')
			);
		$regole = array(
				'titolo_risorsa' => 'required'
				,'descrizione_risorsa' => 'required'
				,'categorie.0' => 'required'
			);

		$validatore = Validator::make($data, $regole);

		if( $validatore->passes() ){
			$file = Input::file('url_risorsa');
			$cartella_random = str_random(2);
			$percorso_destinazione = 'uploads/'. $cartella_random;

			if ( $file ){
				$nome_file = $file->getClientOriginalName();
				Input::file('url_risorsa')->move($percorso_destinazione, $nome_file);
			}
			$quante_categorie =  count(Input::get('categorie'));
			$categorie = NULL;

			for($i=0;$i<$quante_categorie;$i++){
				$categorie .= Input::get('categorie.' . $i) . ',';
			}
			$risorse = new Risorse;
			$risorse->titolo_risorsa = $data['titolo_risorsa'];
			$risorse->descrizione_risorsa = $data['descrizione_risorsa'];
			if ( $file ){
				$risorse->url_risorsa = 'uploads/' . $cartella_random . '/' . $nome_file;
			}
			$risorse->categorie = $categorie;
			$risorse->save();
			return Redirect::action('RisorseController@index');							
		}else{
			 $m =  $validatore->messages();
			 foreach($m->all() as $messaggio){
			 	echo $messaggio . "<br>";
			 }

			 die();
			return Redirect::action('RisorseController@create')->withInput();
		}

		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['risorse_dettaglio'] = Risorse::find($id);
		$this->layout->content = View::make('risorse.show', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['risorse_dettaglio'] = Risorse::find($id);
	    $data['categorie_lista'] = Categorie::all();
    	$data['categorie_scelte'] = explode(',', $data['risorse_dettaglio']->categorie);		
		$this->layout->content = View::make('risorse.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = array(
				'titolo_risorsa' => Input::get('titolo_risorsa')
				,'descrizione_risorsa' => Input::get('descrizione_risorsa')
				,'categorie.0' => Input::get('categorie.0')
			);
		$regole = array(
				'titolo_risorsa' => 'required'
				,'descrizione_risorsa' => 'required'
				,'categorie.0' => 'required'
			);

		$validatore = Validator::make($data, $regole);		
		 
		if ( $validatore->passes() ){
			$file = Input::file('url_risorsa');
	 		if ( ! is_null($file) ){
				$cartella_random = str_random(2);
				$percorso_destinazione = 'uploads/'. $cartella_random;
				$nome_file = $file->getClientOriginalName();
				Input::file('url_risorsa')->move($percorso_destinazione, $nome_file);
			}	 

			$risorse = Risorse::find($id);
			$quante_categorie =  count(Input::get('categorie'));
			$categorie = NULL;

			for($i=0;$i<$quante_categorie;$i++){
				$categorie .= Input::get('categorie.' . $i) . ',';
			}

			$risorse->titolo_risorsa = Input::get('titolo_risorsa');
			$risorse->descrizione_risorsa = Input::get('descrizione_risorsa');
			if ( ! is_null($file) ){
				File::delete($risorse->url_risorsa);
				$risorse->url_risorsa = 'uploads/' . $cartella_random . '/' . $nome_file;
			}
			$risorse->categorie = $categorie;
			$risorse->save();
			return Redirect::action('RisorseController@index');				
		}else{
			return Redirect::action('RisorseController@edit', [$id])->withInput();
		} 

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	 $risorsa = Risorse::find($id);
	 File::delete($risorsa->url_risorsa);
 	 $risorsa->delete();
	 return Redirect::action('RisorseController@index'); 	 
	}

}