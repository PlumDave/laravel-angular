<?php

// =============================================
// HOME PAGE ===================================
// =============================================

// Route::get('/', function()
// {
// 	return View::make('index');
// });
// I use the controller instead this route to se the correct view in "blade", otherwise I get a blank page!

Route::get('/', 'HomeController@showWelcome');
Route::get('login', 'HomeController@login');
Route::post('login_process', array(
							'before' => 'csrf',
							'uses' => 'HomeController@login_process'
							));

Route::get('area_utenti',
				array(
					'before' => 'auth|livello_uno',
         	'uses' => 'HomeController@area_utenti'
				)
);

Route::get('area_risorse_utenti',
				array(
					'before' => 'auth|livello_uno',
					'uses' => 'HomeController@area_risorse_utenti'
				)
);

Route::get('logout', function(){
		Auth::logout();
		Session::flush();
		return Redirect::to('login');
});


Route::group(array('before'=>'auth|livello_uno'), function() {

    // Route::resource('categorie', 'CategorieController');
    Route::get('posts', 'HomeController@showPosts');
    Route::get('categorie', 'HomeController@showCategorie');
		Route::get('tags', 'HomeController@showTags');
		Route::get('linksangular', 'HomeController@showLinks');
		Route::get('utentiangular', 'HomeController@showUtenti');

		Route::resource('utenti', 'UtentiController');
		Route::resource('risorse', 'RisorseController');
		Route::resource('links', 'LinksController');
		Route::resource('tag', 'TagController');

    // =============================================
		// API ROUTES ==================================
		// =============================================
    Route::group(array('prefix' => 'api'), function() {
				// since we will be using this just for CRUD, we won't need create and edit
				// Angular will handle both of those forms
				// this ensures that a user can't access api/create or api/edit when there's nothing there
				Route::resource('comments', 'CommentController',
						array('except' => array('create', 'edit', 'update')));
		});
		Route::group(array('prefix' => 'api'), function() {
				Route::resource('posts', 'PostController',
						array('except' => array('create', 'edit')));
		});
		Route::group(array('prefix' => 'api'), function() {
				Route::resource('tags', 'TagAngularController',
						array('except' => array('create', 'edit')));
		});
		Route::group(array('prefix' => 'api'), function() {
				Route::resource('links', 'LinksAngularController',
						array('except' => array('create', 'edit')));
		});
		Route::group(array('prefix' => 'api'), function() {
				Route::resource('categorie', 'CategoryController',
						array('except' => array('create', 'edit')));
		});
		Route::group(array('prefix' => 'api'), function() {
			Route::resource('utenti', 'UtentiAngularController',
			array('except' => array('create', 'edit')));
		});

});

// Route::get('/', 'HomeController@showComments');

// Route::get('news', 'HomeController@showNews');

// =============================================
// CATCH ALL ROUTE =============================
// =============================================
// all routes that are not home or api will be redirected to the frontend
// this allows angular to route them
App::missing(function($exception)
{
	return View::make('index');
});
