@section('content')
	<div class="container" ng-app="tagApp" ng-controller="mainTagController">

		<div class="row">


				<div class="page-header">
					<h2>Laravel and Angular <b>Double</b> Page Application</h2>
					<h4>Tags System</h4>
				</div>

				<div class="col-lg-8">

				<form ng-submit="submitTag()">

					<!-- AUTHOR -->
					<div class="form-group">
						<input type="text" class="form-control input-sm" name="nome_tag" ng-model="tagData.nome_tag" placeholder="Name">
					</div>

					<!-- SUBMIT BUTTON -->
					<div class="form-group text-right">
						<button type="submit" class="btn btn-primary btn-lg">Submit</button>
					</div>
				</form>
			</div>

			<!-- OBJECT DISPLAY -->
			<div class="col-lg-4">
				<pre>
				<% TagData %>
				</pre>
			</div>

		</div>

		<!-- ALERT -->
		<div class="row">
			<div class="col-md-12">
				<div ng-show="alert.display" class="alert alert-success" role="alert">
					<% alert.message %>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-6 col-md-6">

				<!-- LOADING -->
				<!-- show loading icon if the loading variable is set to true -->
				<p class="text-center" ng-show="loading"><span class="fa fa-meh-o fa-5x fa-spin"></span></p>

				<!-- THE TAGS -->
				<table class="table table-striped tags">
					<tr ng-hide="loading" ng-repeat="tag in tags">
						<td><a ng-click="showTag(tag.id)" ><h3>Tag #<% tag.id %> <small><% tag.nome_tag %></h3></a></td>
						<!-- <td><p><% tag.text %></p></td> -->
						<!-- <td><p><% tag.other_field %></p></td> -->
						<td><p><a ng-click="deleteTag(tag.id)" class="text-muted">Delete</a></p></td>
					</tr>
				</table>

			</div>

			<div class="col-lg-6 col-md-6">

				<form ng-submit="updateTag(singletag)">

					<!-- NOME_TAG -->
					<div class="form-group">
						<input type="text" class="form-control input-sm" name="nome_tag" ng-model="singletag.nome_tag" placeholder="Nome Tag">
					</div>

					<!-- SUBMIT BUTTON -->
					<div class="form-group text-right">
						<button type="submit" class="btn btn-primary btn-lg">Save</button>
					</div>
				</form>

			</div>
		</div>
	</div>
@stop
