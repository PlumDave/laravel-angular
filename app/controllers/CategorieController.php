<?php

class CategorieController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */


	public function index()
	{
		$data['categorie_lista'] = Categorie::all();
		$this->layout->content = View::make('categorie.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
 		$this->layout->content = View::make('categorie.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = array(
			'nome_categoria' => Input::get('nome_categoria'),
			'descrizione_categoria' => Input::get('descrizione')
		);

		$regole = array(
			'nome_categoria' => 'alpha|required'
		);

		$validatore = Validator::make($data, $regole);

		if ($validatore->passes() ) {
			$categorie = new Categorie;
			$categorie->nome_categoria = $data['nome_categoria'];
			$categorie->descrizione_categoria = $data['descrizione_categoria'];
			$categorie->save();

			return Redirect::action('CategorieController@index');
		} else {
			return Redirect::action('CategorieController@create')->withInput();
		}

 		// $categorie = new Categorie;
		// $categorie->nome_categoria = Input::get('nome_categoria');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['categoria_dettaglio'] = Categorie::find($id);
		$this->layout->content = View::make('categorie.show', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['categoria_dettaglio'] = Categorie::find($id);
		$this->layout->content = View::make('categorie.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = array(
			'nome_categoria' => Input::get('nome_categoria'),
			'descrizione_categoria' => Input::get('descrizione')
		);
		$regole = array(
			'nome_categoria' => 'alpha'
		);

		$validatore = Validator::make($data,$regole);

		if ($validatore->passes() ) {
			$categoria = Categorie::find($id);
			$categoria->nome_categoria = $data['nome_categoria'];
			$catgoria->descrizione_categoria = $data['descrizione_categoria'];
			$categoria->save();

			return Redirect::action('CategorieController@index');
		} else {
			return Redirect::action('CategorieController@edit', [$id])->withInput();
		}

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	 $categoria = Categorie::find($id);
 	 $categoria->delete();
	 return Redirect::action('CategorieController@index');
	}

}
