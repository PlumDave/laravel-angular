  <!DOCTYPE html>
<html>
  <head>
    <title>Dave Laravel and Angular</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ url('/bs/css/bootstrap.min.css') }}" media="screen">
    <link rel="stylesheet" href="{{ url('/fa/css/font-awesome.min.css') }}">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.8/angular.min.js"></script> <!-- load angular -->

    <!-- ANGULAR -->
    <!-- all angular resources will be loaded from the /public folder -->
    <script src="{{ url('js/controllers/mainCtrl.js') }}"></script>
    <!-- <script src="{{ url('js/services/commentService.js') }}"></script> -->
    <!-- <script src="{{ url('js/controllers/mainPostCtrl.js') }}"></script> -->
    <!-- <a href=""></a><script src="{{ url('js/services/postService.js') }}"></script> -->
    <!-- <script src="{{ url('js/controllers/mainCategoryCtrl.js') }}"></script> -->
    <script src="{{ url('js/services/Services.js') }}"></script>
    <script src="{{ url('js/app.js') }}"></script> <!-- load our application -->

    <link rel="stylesheet" href="{{ url('/css/style.css') }}">

  </head>
  <body>
      <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Laravel & angular</a>
        <div class="nav-collapse collapse">
          <ul class="nav navbar-nav">

              @if ( ! Session::has('username') )
                <li class="active"><a href="{{ url('/login') }}">Login</a></li>
              @else
                <li class="active"><a href="{{ url('logout') }}">Logout</a></li>
                <li class="active"><a href="{{ url('categorie') }}">Categorie</a></li>
                <li class="active"><a href="{{ url('posts') }}">Posts</a></li>
                <li class="active"><a href="{{ url('utenti') }}">Utenti</a></li>
                <li class="active"><a href="{{ url('links') }}">Links</a></li>
                <li class="active"><a href="{{ url('linksangular') }}">Angular Links</a></li>
                <li class="active"><a href="{{ url('tag') }}">Tag</a></li>
                <li class="active"><a href="{{ url('tags') }}">Angular Tags</a></li>
                <li class="active"><a href="{{ url('risorse') }}">Risorse</a></li>
              @endif

          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          @yield('content')
        </div>
      </div>
    </div>
    <!-- JavaScript plugins (requires jQuery) -->
    <script src="{{ url('http://code.jquery.com/jquery.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ url('/bs/js/bootstrap.min.js') }}"></script>
   </body>
</html>
