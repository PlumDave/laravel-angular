@section('content')

{{ Form::open(array('url' => 'utenti', 'method' => 'POST')) }}
<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		{{ Form::label('username', 'Username') }}
		{{ Form::text('username', '', array('class'=>'form-control')) }}
		</div>
	</div>
</div> 

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		{{ Form::label('password', 'Password') }}
		{{ Form::password('password', array('class'=>'form-control')) }}
		</div>
	</div>
</div> 

<div class="row">
	<div class="col-lg-3">
		{{ Form::label('livello', 'Livello utente') }}
		<br>
		{{ Form::select('livello', $livelli) }}
	</div>
</div>
 
<hr>
 <div class="row">
	<div class="col-lg-3">
	
		<div class="form-group">
	{{ Form::label('categorie', Lang::choice('messages.categoria_label', 1)) }}		    
		 @foreach($categorie_lista as $c)
		<div class="checkbox">		 
		  <label>
   		  {{ Form::checkbox('categorie[]', $c['id'] ) }}		    
		  {{ $c['nome_categoria'] }}
		  </label>		 
		</div>	
		 @endforeach	
		</div>
	</div>
</div>



<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
			{{ Form::submit('Aggiungi questo utente',  array('class' =>'btn btn-success btn-large')) }}
		</div>
	</div>
</div>
{{ Form::close() }}
@stop