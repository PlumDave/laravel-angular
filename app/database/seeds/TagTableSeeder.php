<?php

class TagTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('tag')->truncate();

		$tag = array(
			'nome_tag' => 'news'
		);

		// Uncomment the below to run the seeder
		  DB::table('tag')->insert($tag);
	}

}
