@section('content')

{{ Form::open(array('url' => 'login_process', 'method' => 'post')) }}
<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		{{ Form::label('username', 'Username') }}
		{{ Form::text('username', '', array('class'=>'form-control')) }}
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		{{ Form::label('password','Password') }}
		{{ Form::password('password',  array('class'=>'form-control')) }}
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
			{{ Form::submit('Login!',  array('class' =>'btn btn-success btn-large')) }}
		</div>
	</div>
</div>
{{ Form::close() }}
@stop