<?php

class Category extends Eloquent {

	protected $table = 'categorie';
  protected $fillable = array('nome_categoria', 'descrizione_categoria');

}
