@section('content')
<div class="row">
	<div clas="col-lg-6">
		<div class="form-group">
			<h3>Sei sicurao di voler cancellare questo utente?</h3>
		</div>
	</div>
</div>

{{ Form::open(array('url' => 'utenti/'. $utente_dettaglio->id, 'method' => 'DELETE' )) }}
<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
			{{ $utente_dettaglio->username }}
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
			{{ Form::submit('Cancella questo utente', array('class' => 'btn btn-success' )) }}
		</div>
	</div>

	<div class="col-lg-3">
		<a href="{{ url('utenti') }}" class="btn btn-warning btn-large">No, ci ho ripensato</a>
	</div>
</div>
{{ Form::close() }}
@stop