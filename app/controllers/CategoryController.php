<?php

class CategoryController extends \BaseController {

	/**
	 * Send back all categorie as JSON
	 *
	 * @return Response
	 */
	public function index()
	{
		return Response::json(Category::get());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		Category::create(array(
			'nome_categoria' => Input::get('nome_categoria'),
			'descrizione_categoria' => Input::get('descrizione_categoria'),
		));

		return Response::json(array('success' => true));
	}

	/**
	 * Return the specified resource using JSON
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Response::json(Category::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
	 $paramId = Input::get('id');

	 $categoria = Categorie::find($id);
	 $categoria->nome_categoria = Input::get('nome_categoria');
	 $categoria->descrizione_categoria = Input::get('descrizione_categoria');
	 $categoria->save();

	 return Response::json(array('success' => true));

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Category::destroy($id);

		return Response::json(array('success' => true));
	}
}
