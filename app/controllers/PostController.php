<?php

class PostController extends \BaseController {

	/**
	 * Send back all posts as JSON
	 *
	 * @return Response
	 */
	public function index()
	{
		return Response::json(Post::get());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		Post::create(array(
			'author' => Input::get('author'),
			'text' => Input::get('text'),
			'other_field' => Input::get('other_field'),
		));

		return Response::json(array('success' => true));
	}

	/**
	 * Return the specified resource using JSON
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Response::json(Post::find($id));
	}

	/**
	* Update the specified resource in storage.
	*
	* @param  int  $id
	* @return Response
	*/
	public function update($id)
	{
	$paramId = Input::get('id');

	$post = Post::find($id);
	$post->text = Input::get('text');
	$post->author = Input::get('author');
	$post->other_field = Input::get('other_field');
	$post->save();

	return Response::json(array('success' => true));

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Post::destroy($id);

		return Response::json(array('success' => true));
	}

}
