@section('content')
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
		<h3>Sei sicuro di voler cancellare questa risorsa?</h3>
		</div>
	</div>
</div> 

{{ Form::open(array('url' => 'risorse/'. $risorse_dettaglio->id, 'method' => 'DELETE')) }}
<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
			 {{ $risorse_dettaglio->titolo_risorsa }}
		</div>
	</div>
</div> 

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
			{{ Form::submit('Cancella questa risorsa',  array('class' =>'btn btn-success btn-large')) }}
			
		</div>
	</div>

	<div class="col-lg-3">
		<a href="{{ url('risorse') }}" class="btn btn-warning btn-large">No, ci ho ripensato</a>
	</div>
</div>
{{ Form::close() }}
@stop