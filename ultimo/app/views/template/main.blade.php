  <!DOCTYPE html>
<html>
  <head>
    <title>Bootstrap 101 Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ url('/bs/css/bootstrap.min.css') }}" media="screen">
    <link rel="stylesheet" href="{{ url('/css/custom.css') }}" media="screen">
    <style type="text/css">
      body {
        padding-top: 70px;
      }
      </style>
  </head>
  <body>
      <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Intranet Project</a>
        <div class="nav-collapse collapse">
          <ul class="nav navbar-nav">
           
                @if ( ! Session::has('username') )               
  <li class="active"><a href="{{ url('/login') }}">Login</a></li>
               @else                    
<li class="active"><a href="{{ url('logout') }}">Logout</a></li> 
              @if ( Auth::user()->livello == 0 ) 
                <li class="active"><a href="{{ url('categorie') }}">Categorie</a></li>
                <li class="active"><a href="{{ url('utenti') }}">Utenti</a></li>
                <li class="active"><a href="{{ url('links') }}">Links</a></li>
                <li class="active"><a href="{{ url('tag') }}">Tag</a></li>
              @endif
<li class="active"><a href="{{ url('risorse') }}">Risorse</a></li>

               @endif  
            
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-lg-12">
     @yield('content')
        </div>
      </div>
    </div>
    <!-- JavaScript plugins (requires jQuery) -->
    <script src="{{ url('http://code.jquery.com/jquery.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ url('/bs/js/bootstrap.min.js') }}"></script>
   </body>
</html>