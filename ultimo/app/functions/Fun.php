<?php 

class Fun {
    public static function numeri_romani($testo) {
        $lettere = array('prima', 'seconda', 'terza', 
        				'quarta', 'quinta', 'sesta'
        				,'settima', 'ottava', 'nona', 'decima');
    	$romani = array('I', 'II', 'III', 
    					'IV', 'V', 'VI'
    					, 'VII', 'VIII', 'IX' ,'X');

        return str_replace($lettere, $romani, $testo); 
    }
}