@section('content')
<div class="row">
	<div class="col-lg-12">
		<h1>Benvenuto nella tua area riservata</h1>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<h3>Ecco le risorse a tua disposizione</h3>
	</div>
</div>

<div class="row">
<table class="table table-striped">
 
   	    @foreach($risorse as $r)

		  	<?php 
		  	$categorie_risorse = explode(',', $r['categorie']);

		  	 ?>  	 
		  
		  	@if( count( array_intersect( array_filter($categorie), array_filter($categorie_risorse)) ) > 0) 
		  		 
		  		<tr>
		  			<td>{{ $r['titolo_risorsa'] }}</td>
		  			<td>{{ $r['descrizione_risorsa'] }}</td>
		  			<td><a href="/{{ $r['url_risorsa']}}">Apri il documento</a></td>
		  		</tr>
		  	@endif  
		@endforeach
 </table>
</div>
@stop