@section('content')
<div class="container" ng-app="categoryApp" ng-controller="mainCategoryController">

		<div class="row">
				<div class="page-header">
					<h2>Laravel and Angular <b>Double</b> Page Application</h2>
					<h4>Categories System</h4>
				</div>

				<div class="col-lg-8">
				<!-- NEW CATEGORY FORM -->
				<form ng-submit="submitCategory()"><!-- ng-submit will disable the default form action and use our function -->
					<!-- AUTHOR -->
					<div class="form-group">
						<input type="text" class="form-control input-sm" name="author" ng-model="categoryData.nome_categoria" placeholder="Name">
					</div>
					<!-- CATEGORY TEXT -->
					<div class="form-group">
						<input type="text" class="form-control input-lg" name="category_other" ng-model="categoryData.descrizione_categoria" placeholder="Other input">
					</div>
					<!-- SUBMIT BUTTON -->
					<div class="form-group text-right">
						<button type="submit" class="btn btn-primary btn-lg">Create new</button>
					</div>
				</form>
			</div>

			<div class="col-lg-4">
				<pre>
				<% categoryData %>
				</pre>
			</div>

			<div class="col-md-12">
				<div ng-show="alert.display" class="alert alert-success" role="alert">
					<% alert.message %>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-lg-8 col-md-8">
				<!-- LOADING ICON -->
				<p class="text-center" ng-show="loading"><span class="fa fa-meh-o fa-5x fa-spin"></span></p>

				<!-- THE CATEGORIES -->
				<table class="table table-striped categorie">
					<tr ng-hide="loading" ng-repeat="cat in categorie">
						<td><h3><a ng-click="showCategory(cat.id)" >#<% cat.id %><a><small>by <% cat.nome_categoria %></h3></td>
						<td><p><% cat.descrizione_categoria %></p></td>
						<td><p><a href="#" ng-click="deleteCategory(cat.id)" class="text-muted">Delete</a></p></td>
					</tr>
				</table>
			</div>

			<div class="col-lg-4 col-md-4">
				<form ng-submit="updateCategory(singlecategory)"> <!-- ng-submit will disable the default form action and use our function -->
					<!-- AUTHOR -->
					<div class="form-group">
						<input type="text" class="form-control input-sm" name="author" ng-model="singlecategory.nome_categoria" placeholder="Name">
					</div>
					<!-- CATEGORY TEXT -->
					<div class="form-group">
						<textarea type="text" class="form-control input-lg" name="category_other" ng-model="singlecategory.descrizione_categoria" placeholder="Other input"></textarea>
					</div>
					<!-- SUBMIT BUTTON -->
					<div class="form-group text-right">
						<button type="submit" class="btn btn-primary btn-lg">Save</button>
					</div>
				</form>
			</div>

		</div>
	</div>
@stop
