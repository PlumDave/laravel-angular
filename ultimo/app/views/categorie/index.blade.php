@section('content')
<div class="row">
	<div class="col-lg-12">
		<span class="pull-right">
			<a href="{{ url('categorie/create') }}" class="btn btn-success">
				Nuova categoria
			</a>
		</span>
	</div>
</div>

<hr>

<div class="row">
	<div class="col-lg-12">
		<table class="table table-striped">
		<tr class="success">
			<td>Categoria</td>
			<td>Azioni</td>
		</tr>
		@foreach($categorie_lista as $c)
		<tr>
			<td>{{ $c['nome_categoria'] }}</td>
			<td>
			<a href="{{ url('categorie/'.$c['id'].'/edit') }}" class="btn btn-warning">Modifica</a> 
			<a href="{{ url('categorie/'.$c['id']) }}" class="btn btn-warning">Cancella</a>
			</td>
		</tr>
		@endforeach
		</table>
	</div>
</div>
@stop