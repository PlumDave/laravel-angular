<?php

class Categorie extends Eloquent {

    protected $table = 'categorie';
    protected $fillable = array('nome_categoria', 'descrizione_categoria');	

}