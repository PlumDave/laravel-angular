@section('content')

{{ Form::open(array('url' => 'tag', 'method' => 'POST')) }}
<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		{{ Form::label('nome_tag', 'Nome tag') }}
		{{ Form::text('nome_tag', '', array('class'=>'form-control')) }}
		</div>
	</div>
</div> 
 

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
			{{ Form::submit('Aggiungi questo tag',  array('class' =>'btn btn-success btn-large')) }}
		</div>
	</div>
</div>
{{ Form::close() }}
@stop