@section('content')

{{ Form::open(array('url' => 'risorse', 'method' => 'POST', 'files' => true )) }}
<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		{{ Form::label('titolo_risorsa', 'Titolo risorsa') }}
		{{ Form::text('titolo_risorsa', '', array('class'=>'form-control')) }}
		</div>
	</div>
</div> 

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		{{ Form::label('descrizione_risorsa', 'Descrizione risorsa') }}
		{{ Form::textarea('descrizione_risorsa', '', array('class'=>'form-control')) }}
		</div>
	</div>
</div> 

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		{{ Form::label('url_risorsa', 'URL risorsa') }}
		{{ Form::file('url_risorsa', '', array('class'=>'form-control')) }}
		</div>
	</div>
</div> 

 <div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		 @foreach($categorie_lista as $c)
		<div class="checkbox">		 
		  <label>
		  {{ Form::checkbox('categorie[]', $c['id']) }}		    
		  {{ $c['nome_categoria'] }}
		  </label>		 
		</div>	
		 @endforeach	
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
			{{ Form::submit('Aggiungi questa risorsa',  array('class' =>'btn btn-success btn-large')) }}
		</div>
	</div>
</div>
{{ Form::close() }}
@stop