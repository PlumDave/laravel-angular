@section('content')
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
		<h3>Sei sicuro di voler cancellare questo link?</h3>
		</div>
	</div>
</div> 

{{ Form::open(array('url' => 'links/'. $link_dettaglio->id, 'method' => 'DELETE')) }}
<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
			 {{ $link_dettaglio->nome_link }}
		</div>
	</div>
</div> 

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
			{{ Form::submit('Cancella questo link',  array('class' =>'btn btn-success btn-large')) }}
			
		</div>
	</div>

	<div class="col-lg-3">
		<a href="{{ url('links') }}" class="btn btn-warning btn-large">No, ci ho ripensato</a>
	</div>
</div>
{{ Form::close() }}
@stop