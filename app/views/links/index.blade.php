@section('content')
<div class="row">
	<div class="col-lg-12">
		<span class="pull-right">
			<a href="{{ url('links/create') }}" class="btn btn-success">
				Nuovo Link
			</a>
		</span>
	</div>
</div>

<hr>

<div class="row">
	<div class="col-lg-12">
		<table class="table table-striped">
		<tr class="success">
			<td>Link</td>
			<td>Tags</td>
			<td>Azioni</td>
		</tr>
		@foreach($links_lista as $c)
		<tr>
			<td>{{ $c['nome_link'] }}</td>
			<td>
				@foreach($c['tags'] as $d)
					{{ $d['nome_tag'] }}
				@endforeach
			</td>
			<td>
			<a href="{{ url('links/'.$c['id'].'/edit') }}" class="btn btn-warning">Modifica</a>
			<a href="{{ url('links/'.$c['id']) }}" class="btn btn-warning">Cancella</a>
			</td>
		</tr>
		@endforeach
		</table>
	</div>
</div>
@stop
