@section('content')
<div class="row">
	<div class="col-lg-12">
		<span class="pull-right">
			<a href="{{ url('risorse/create') }}" class="btn btn-success">
				Nuova risorsa
			</a>
		</span>
	</div>
</div>

<hr>

<div class="row">
	<div class="col-lg-12">
		<table class="table table-striped">
		<tr class="success">
			<td>Risorsa</td>
			<td>Azioni</td>
		</tr>
		@foreach($risorse_lista as $r)
		<tr>
			<!-- Fun::numeri_romani() -->
			<td>{{ Fun::numeri_romani($r['titolo_risorsa']) }}</td>
			<td>
			<a href="{{ url('risorse/'.$r['id'].'/edit') }}" class="btn btn-warning">Modifica</a>
			<a href="{{ url('risorse/'.$r['id']) }}" class="btn btn-warning">Cancella</a>
			</td>
		</tr>
		@endforeach
		</table>

		{{ $risorse_lista->links(); }}
	</div>
</div>
@stop
