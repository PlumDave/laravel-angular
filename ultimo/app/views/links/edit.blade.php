@section('content')

{{ Form::open(array('url' => 'links/'. $link_dettaglio->id, 'method' => 'PUT')) }}
<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		{{ Form::label('nome_link', 'Nome link') }}
		{{ Form::text('nome_link', $link_dettaglio->nome_link, array('class'=>'form-control')) }}
		</div>
	</div>
</div> 


<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		{{ Form::label('url', 'URL') }}
		{{ Form::text('url', $link_dettaglio->url, array('class'=>'form-control')) }}
		</div>
	</div>
</div> 

 <div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		
		{{ Form::label('tag', 'Scegli uno o più tag') }}	
		 @foreach($tag_lista as $c)
		  
		<div class="checkbox">		 
		  <label>
		  {{ Form::checkbox('tag[]', $c['id'] , FALSE) }}		    
		  {{ $c['nome_tag'] }}
		  </label>		 
		</div>	
		 @endforeach	
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
			{{ Form::submit('Aggiorna questo link',  array('class' =>'btn btn-success btn-large')) }}
		</div>
	</div>
</div>
{{ Form::close() }}
@stop