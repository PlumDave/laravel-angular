<?php

class TagAngularController extends \BaseController {

	/**
	 * Send back all tags as JSON
	 *
	 * @return Response
	 */
	public function index()
	{
		return Response::json(AngularTag::get());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		AngularTag::create(array(
			'nome_tag' => Input::get('nome_tag'),
		));

		return Response::json(array('success' => true));
	}

	/**
	 * Return the specified resource using JSON
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Response::json(AngularTag::find($id));
	}

	/**
	* Update the specified resource in storage.
	*
	* @param  int  $id
	* @return Response
	*/
	public function update($id)
	{
	$paramId = Input::get('id');

	$tag = AngularTag::find($id);
	$tag->nome_tag = Input::get('nome_tag');
	$tag->save();

	return Response::json(array('success' => true));

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		AngularTag::destroy($id);

		return Response::json(array('success' => true));
	}

}
